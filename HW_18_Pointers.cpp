#include <iostream>
#include <string>

class Player {

public:
	std::string PlayerName;
	int PlayerScore;

	Player() : PlayerScore(0) {};
	Player(std::string name, int score) : PlayerName(name), PlayerScore(score) {};

};
//create quick sort algorhitm
void swap(Player arr[], int pos1, int pos2) {
	Player temp;
	temp = arr[pos1];
	arr[pos1] = arr[pos2];
	arr[pos2] = temp;
}
int partition(Player arr[], int low, int high, int pivot) {
	int i = low;
	int j = low;
	while (i <= high) {
		if (arr[i].PlayerScore > pivot) {
			i++;
		}
		else {
			swap(arr, i, j);
			i++;
			j++;
		}
	}
	return j - 1;
}
void quickSort(Player arr[], int low, int high) {
	if (low < high) {
		int pivot = arr[high].PlayerScore;
		int pos = partition(arr, low, high, pivot);

		quickSort(arr, low, pos - 1);
		quickSort(arr, pos + 1, high);
	}
}

int main() {

	setlocale(LC_ALL, "Russian");

	bool flag = true;
	std::string N;
	int NumberOfPlayers = NULL;
	//input number of players
	while (flag)
	{
		std::cout << "Please, enter number of players in range between 0 and 29:" << std::endl;
		std::cin >> N;
		for (int i = 0; i <= N.length(); i++) {
			if (i == N.length()) {
				NumberOfPlayers = std::stoi(N);
				if (NumberOfPlayers > 0 && NumberOfPlayers < 30) {
					std::cout << "Great, number of players is: " << NumberOfPlayers << std::endl;
					flag = false;
					break;
				}
				else
					std::cout << "Error, You need to input positive and integer number" << std::endl;
			}
			else if (isdigit(N[i]))
				continue;
			else {
				std::cout << "Incorrect input, please be sure, that you enter positive and interger number" << std::endl;
				break;
			}
		}

	}
	//create array of class with players
	Player* arrayPlayers_Scores = new Player[NumberOfPlayers];

	for (int i = 0; i < NumberOfPlayers; ++i) {
		std::string PlayerName;
		int Player_Scores;

		std::cout << "Input name of player: " << i + 1 << ": " << std::endl;
		std::cin >> PlayerName;
		while (true)
		{
			std::cout << "Input value of score for this player: " << std::endl;
			std::cin >> Player_Scores;
			std::cout << std::endl;
			if (std::cin.fail()) {
				std::cin.clear();
				std::cin.ignore(std::numeric_limits <std::streamsize>::max(), '\n');
				std::cout << "Error of input, you should to enter integer value" << std::endl;
			}
			else
			{
				break;
			}
		}

		arrayPlayers_Scores[i] = Player(PlayerName, Player_Scores);
	}

	/*std::cout << "unsorted massive" << std::endl;
	for (int i = 0; i < NumberOfPlayers; ++i) {
		std::cout << "The player number " << i + 1 << " who has name " << arrayPlayers_Scores[i].PlayerName << " just earned " << arrayPlayers_Scores[i].PlayerScore << " scores!\n";
	}*/

	//sorting array
	quickSort(arrayPlayers_Scores, 0, NumberOfPlayers - 1);

	for (int i = 0; i < NumberOfPlayers; ++i) {
		std::cout << "The player number " << i + 1 << " who has name " << arrayPlayers_Scores[i].PlayerName << " just earned " << arrayPlayers_Scores[i].PlayerScore << " scores!\n";
	}

	delete[] arrayPlayers_Scores;
	return 0;
}